#include <iostream>

using namespace std;

class Stack
{

private:
	int size;
	int* data;

public:

	Stack()
	{
		size = 0;
	}


	// ����� ��� �������� ������������� ������� 
	//� ��� ����������. 
	Stack(int size)
	{
		this->size = size;

		data = new int[size];

		for (int i = 0; i < size; i++)
		{
			data[i] = i;			
		}

	}
	//����� ����� ������ � ��������
	//���������� ���������� ��� ���� ������ � ��.
	~Stack()
	{
		delete[] data;
	}

	//�������� �������� ������, ����� ��������
	//����� �.�. � �������� ������.

	Stack(const Stack& other)
	{

		this->size = other.size;

		for (int i = 0; i < size; i++)
		{
			this->data[i] = other.data[i];
		}

	}
	// ������, ��� ����������� ����� pop
	// ��������� - �� ���� �� ����.
	void pop()
	{
		if (size != 0)
		{
			size--;
		}
		else
		{
			cout << "��������, ���� ��� ����\n";
		}
	}

	void push()
	{
		size++;
	}

	void print()
	{
		for (int i = 0; i < size; i++)
		{
			cout << i << " ";
		}
	}

};
